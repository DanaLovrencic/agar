#version 460 core

layout(location = 0) out vec4 color;

in vec2  v_coordinate;
in vec2  v_center;
in float v_radius;
in vec4  v_color;

void main()
{
    if (distance(v_coordinate, v_center) < 0.9 * v_radius)
        color = vec4(v_color.r + 0.01, v_color.g + 0.01, v_color.b + 0.01, 1.0);
    else if (distance(v_coordinate, v_center) >= 0.9 * v_radius && distance(v_coordinate, v_center) < v_radius)
        color = v_color;
    else
        color = vec4(0.0);
    /*color = distance(v_coordinate, v_center) >= v_radius / 3.0 && distance(v_coordinate, v_center) < v_radius ?  v_color : vec4(0.0);*/
    /*color = distance(v_coordinate, v_center) < v_radius / 3.0 ?  vec4(0.2, 0.8, 0.2, 1.0): vec4(0.0);*/
}
