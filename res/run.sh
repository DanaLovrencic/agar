#!/bin/sh

BUILD_ROOT="$1"
SOURCE_ROOT="$2"
[ -d "$BUILD_ROOT" ]  || { echo "Invalid build root $BUILD_ROOT."; exit 1; }
[ -d "$SOURCE_ROOT" ] || { echo "Invalid source root $SOURCE_ROOT."; exit 1; }

"$BUILD_ROOT/Server" --font "$SOURCE_ROOT/res/fonts/Hack-Regular.ttf" \
                     --shader "$SOURCE_ROOT/res/shaders/" \
                     --x 30 \
                     --y 250 \
                     --tickrate 60 \
                     --delay 100 &

sleep 1

"$BUILD_ROOT/Client" --font "$SOURCE_ROOT/res/fonts/Hack-Regular.ttf" \
                     --name "Uzi" \
                     --shader "$SOURCE_ROOT/res/shaders/" \
                     --x 650 \
                     --y 250 \
                     --delay 100 \
                     --polling_rate 60 \
                     --keyboard_option "arrows" &

"$BUILD_ROOT/Client" --font "$SOURCE_ROOT/res/fonts/Hack-Regular.ttf" \
                     --name "Tuzi" \
                     --shader "$SOURCE_ROOT/res/shaders/" \
                     --x 1270 \
                     --y 250 \
                     --delay 100 \
                     --polling_rate 60 \
                     --keyboard_option "arrows" &
