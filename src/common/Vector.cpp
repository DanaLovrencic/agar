#include "Vector.hpp"

std::ostream& operator<<(std::ostream& os, const glm::vec2& v)
{
    os << "(" << v.x << ", " << v.y << ")";
    return os;
}

std::ostream& operator<<(std::ostream& os, const glm::vec4& v)
{
    os << "(" << v.g << ", " << v.g << ", " << v.b << ", " << v.a << ")";
    return os;
}
