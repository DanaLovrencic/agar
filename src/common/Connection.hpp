#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include "Message.hpp"

#include <boost/asio.hpp>

#include <string>
#include <memory>
#include <functional>

/* Enables raw data transfer and provides data storage for the duration of a
 * connection. Messages that are transferred using the connection object consist
 * of a fixed length header and variable length body. Shared pointers to objects
 * of this class passed to the callback functions allow ensure that the data
 * buffers registered to be sent and received are kept alive during the whole
 * lifetime of the connection (until all handlers have been called). Each
 * message sent using this class consists of fixed length header containing the
 * length of the data in hexadecimal and the data itself. */
class Connection : public std::enable_shared_from_this<Connection> {
  public:
    using WriteHandler =
        std::function<void(std::shared_ptr<Connection> connection)>;
    using ReadHandler = std::function<void(
        std::shared_ptr<Connection> connection, std::string message)>;

  private:
    using ErrorCode = boost::system::error_code;
    using Socket    = boost::asio::ip::tcp::socket;
    using Executor  = boost::asio::io_context::executor_type;

    Socket socket;

    Message inbound_wire_message;
    Message outbound_wire_message;

  public:
    /* Create a new connection object that is tied to the provided executor.
     * Note that the create connection object does not represent active
     * connection (it is not yet connected to some socket). */
    explicit Connection(const Executor& executor);

    /* Get the underlying socket. Used for making a connection or for accepting
     * an incoming connection. */
    Socket& get_socket() noexcept;

    /* Asynchronously write the message to the socket. Call the provided handler
     * when the operation has been completed. */
    void async_write(std::string message, WriteHandler handler);

    /* Asynchronously read the message from the socket. Call the provided
     * handler when the operation has been completed. The caller must ensure
     * that the inbound data is kept alive until the handler is called. */
    void async_read(ReadHandler handler);

    /* Synchronously write the message to the socket. */
    void write(std::string message);

    /* Synchronously read the message from the socket. */
    std::string read();

  private:
    /* Handle a completed read of a message header. */
    void handle_read_header(ReadHandler handler);
};

#endif
