#ifndef UTILITY_HPP
#define UTILITY_HPP

#include "Assert.hpp"

/** Function for defining bitfields. Bit that fit into standard unsigned type
 * are supported. */
constexpr unsigned bitfield(unsigned char x)
{
    ASSERT(x < std::numeric_limits<unsigned>::digits,
           "Bit ouside the bitfield range.");
    return 1U << x;
}

#endif
