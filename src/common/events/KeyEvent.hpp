#ifndef KEYEVENT_HPP
#define KEYEVENT_HPP

#include "events/Event.hpp"

/* Base class for all key events. */
class KeyEvent : public Event {
  private:
    int keycode;

  public:
    int get_keycode() const noexcept;

  protected:
    /* Key event constructor. Protected visibility is needed to ensure that
     * instance of this class cannot be constructed. Only inheriting classes can
     * invoke this constructor. */
    KeyEvent(Type type, int keycode) noexcept;
};

class KeyPressedEvent : public KeyEvent {
  private:
    unsigned repeat_count; // Number of times key has been repeated.

  public:
    KeyPressedEvent(int keycode, int repeat_count) noexcept;
    unsigned get_repeat_count() const noexcept;
    std::string get_information() const noexcept override;
    static Type get_static_type() noexcept;
};

class KeyReleasedEvent : public KeyEvent {
  public:
    explicit KeyReleasedEvent(int keycode) noexcept;
    std::string get_information() const noexcept override;
    static Type get_static_type() noexcept;
};

class KeyTypedEvent : public KeyEvent {
  public:
    explicit KeyTypedEvent(int keycode) noexcept;
    std::string get_information() const noexcept override;
    static Type get_static_type() noexcept;
};

#endif
