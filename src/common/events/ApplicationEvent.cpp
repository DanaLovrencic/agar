#include "events/ApplicationEvent.hpp"

WindowResizeEvent::WindowResizeEvent(unsigned width, unsigned height) noexcept
    : Event(get_static_type(), Category::Application),
      width(width),
      height(height)
{}

unsigned WindowResizeEvent::get_width() const noexcept
{
    return width;
}

unsigned WindowResizeEvent::get_height() const noexcept
{
    return height;
}

std::string WindowResizeEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), width, height);
}

Event::Type WindowResizeEvent::get_static_type() noexcept
{
    return Type::WindowResize;
}

WindowCloseEvent::WindowCloseEvent() noexcept
    : Event(get_static_type(), Category::Application)
{}

Event::Type WindowCloseEvent::get_static_type() noexcept
{
    return Type::WindowClose;
}

AppTickEvent::AppTickEvent() noexcept
    : Event(get_static_type(), Category::Application)
{}

Event::Type AppTickEvent::get_static_type() noexcept
{
    return Type::AppTick;
}

AppUpdateEvent::AppUpdateEvent() noexcept
    : Event(get_static_type(), Category::Application)
{}

Event::Type AppUpdateEvent::get_static_type() noexcept
{
    return Type::AppUpdate;
}

AppRenderEvent::AppRenderEvent() noexcept
    : Event(get_static_type(), Category::Application)
{}

Event::Type AppRenderEvent::get_static_type() noexcept
{
    return Type::AppRender;
}
