#include "events/MouseEvent.hpp"

MouseMovedEvent::MouseMovedEvent(double x, double y) noexcept
    : Event(get_static_type(), Category::Mouse | Category::Input), x(x), y(y)
{}

double MouseMovedEvent::get_x() const noexcept
{
    return x;
}

double MouseMovedEvent::get_y() const noexcept
{
    return y;
}

std::string MouseMovedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), x, y);
}

Event::Type MouseMovedEvent::get_static_type() noexcept
{
    return Type::MouseMoved;
}

MouseScrolledEvent::MouseScrolledEvent(double x_offset,
                                       double y_offset) noexcept
    : Event(get_static_type(), Category::Mouse | Category::Input),
      x_offset(x_offset),
      y_offset(y_offset)
{}

double MouseScrolledEvent::get_x_offset() const noexcept
{
    return x_offset;
}

double MouseScrolledEvent::get_y_offset() const noexcept
{
    return y_offset;
}

std::string MouseScrolledEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), x_offset, y_offset);
}

Event::Type MouseScrolledEvent::get_static_type() noexcept
{
    return Type::MouseScrolled;
}

int MouseButtonEvent::get_button() const noexcept
{
    return button;
}

MouseButtonEvent::MouseButtonEvent(Type type, int button) noexcept
    : Event(type, Category::Mouse | Category::Input), button(button)
{}

MouseButtonPressedEvent::MouseButtonPressedEvent(int button) noexcept
    : MouseButtonEvent(get_static_type(), button)
{}

std::string MouseButtonPressedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_button());
}

Event::Type MouseButtonPressedEvent::get_static_type() noexcept
{
    return Type::MouseButtonPressed;
}

MouseButtonReleasedEvent::MouseButtonReleasedEvent(int button) noexcept
    : MouseButtonEvent(get_static_type(), button)
{}

std::string MouseButtonReleasedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_button());
}

Event::Type MouseButtonReleasedEvent::get_static_type() noexcept
{
    return Type::MouseButtonReleased;
}
