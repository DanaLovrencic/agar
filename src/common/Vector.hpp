#ifndef GLM_VECTOR_HPP
#define GLM_VECTOR_HPP

#include <glm/glm.hpp>
#include <ostream>

std::ostream& operator<<(std::ostream& os, const glm::vec2& v);
std::ostream& operator<<(std::ostream& os, const glm::vec4& v);

#endif
