#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Key.hpp"
#include "events/Event.hpp"

#include <string>
#include <filesystem>
#include <utility>

class Window final {
  private:
    GLFWwindow* window;
    std::string title;
    unsigned width;
    unsigned height;
    std::function<void(Event&)> event_handler;
    bool vsync;

  public:
    Window(
        std::string title,
        unsigned width,
        unsigned height,
        std::filesystem::path font_path,
        std::function<void(Event&)> event_handler = [](Event& /*event*/) {},
        bool vsync                                = true) noexcept;
    ~Window() noexcept;

    Window& operator=(const Window& other) = delete;
    Window(const Window& other)            = delete;

    Window(Window&& other) noexcept;
    Window& operator=(Window&& other) noexcept;

    void set_event_handler(std::function<void(Event&)> event_handler) noexcept;

    void on_update() const noexcept;
    void poll_events() const noexcept;

    unsigned get_width() const noexcept;
    unsigned get_height() const noexcept;

    bool is_minimized() const noexcept;

    void set_vsync(bool vsync) noexcept;
    bool is_vsync_on() const noexcept;

    std::pair<unsigned, unsigned> dimensions() const noexcept;

    GLFWwindow* get_native_handle() const noexcept;

    void set_position(int x, int y);

  private:
    void set_glfw_callbacks() const noexcept;

    void size_callback(unsigned new_width, unsigned new_height);
    void close_callback();
    void key_callback(int key, int action);
    void char_callback(unsigned keycode);
    void mouse_button_callback(int button, int action);
    void mouse_scroll_callback(double x_offset, double y_offset);
    void mouse_move_callback(double x_position, double y_position);
};

#endif

