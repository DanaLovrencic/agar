#include "Window.hpp"
#include "Assert.hpp"

#include "events/KeyEvent.hpp"
#include "events/MouseEvent.hpp"
#include "events/ApplicationEvent.hpp"

#include <imgui.h>
#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_opengl3.h>

namespace {
void error_callback(int error, const char* description) noexcept;
}

Window::Window(std::string title,
               unsigned width,
               unsigned height,
               std::filesystem::path font_path,
               std::function<void(Event&)> event_handler,
               bool vsync) noexcept
    : title(std::move(title)),
      width(width),
      height(height),
      event_handler(std::move(event_handler)),
      vsync(vsync)
{
    static bool glfw_initialized = false;
    if (!glfw_initialized) { // Initialize GLFW on first window creation.
        [[maybe_unused]] auto status = glfwInit();
        ASSERT_FMT(
            status, "Failed to initialize GLFW for window {}.", this->title);
        glfwSetErrorCallback(error_callback);
        glfw_initialized = true;
    }

    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
    window = glfwCreateWindow(static_cast<int>(this->width),
                              static_cast<int>(this->height),
                              this->title.c_str(),
                              nullptr,
                              nullptr);
    glfwSetWindowPos(window, 10, 200); // TODO

    // Make OpenGL draw to this window (set the graphics context).
    glfwMakeContextCurrent(window);
    [[maybe_unused]] GLenum status = glewInit();
    ASSERT_FMT(status == GLEW_OK,
               "Failed to initialize GLEW for window {}. Message: {}.",
               this->title,
               glewGetErrorString(status));

    // Set the user data pointer which can be retrieved inside the GLFW callback
    // (handler) functions. This pointer is set to enable access to attributes
    // and member functions.
    glfwSetWindowUserPointer(window, this);

    set_glfw_callbacks(); // Register callback functions for the window.

    // Enable/disable vertical synchronization.
    set_vsync(vsync);

    // Enable/disable blending.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // IMGUI ----------------------------------------
    // Setup ImGui context.
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable keyboard.
    float font_size = dimensions().first / 37.0f;
    io.Fonts->AddFontFromFileTTF(font_path.string().data(), font_size);

    ImGui::StyleColorsLight();

    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags) {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 0.5f;
    }
    ImGui::PushStyleColor(ImGuiCol_WindowBg, 1);
    style.Colors[ImGuiCol_WindowBg].w = 0.0f;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 410");
    // IMGUI =========================================
}

Window::~Window() noexcept
{
    if (window) { glfwDestroyWindow(window); }
}

Window::Window(Window&& other) noexcept
    : window(other.window),
      title(std::move(other.title)),
      width(other.width),
      height(other.height),
      event_handler(std::move(other.event_handler)),
      vsync(other.vsync)
{
    other.window = nullptr;

    // GLFW callbacks should now get pointer to this object, not the other.
    glfwSetWindowUserPointer(window, this);
}

Window& Window::operator=(Window&& other) noexcept
{
    if (window) { glfwDestroyWindow(window); }

    window        = other.window;
    title         = std::move(other.title);
    width         = other.width;
    height        = other.height;
    event_handler = std::move(other.event_handler);
    vsync         = other.vsync;

    other.window = nullptr; // Remove ownership of GLFW window from other.

    // GLFW callbacks should now get pointer to this object, not the other.
    glfwSetWindowUserPointer(window, this);

    return *this;
}

void Window::set_event_handler(
    std::function<void(Event&)> event_handler) noexcept
{
    this->event_handler = std::move(event_handler);
}

void Window::on_update() const noexcept
{
    glfwSwapBuffers(window);
}

void Window::poll_events() const noexcept
{
    glfwPollEvents();
}

unsigned Window::get_width() const noexcept
{
    return width;
}

unsigned Window::get_height() const noexcept
{
    return height;
}

bool Window::is_minimized() const noexcept
{
    return get_width() == 0 || get_height() == 0;
}

void Window::set_vsync(bool vsync) noexcept
{
    this->vsync = vsync;
    if (this->vsync) {
        glfwSwapInterval(1);
    } else {
        glfwSwapInterval(0);
    }
}

bool Window::is_vsync_on() const noexcept
{
    return vsync;
}

GLFWwindow* Window::get_native_handle() const noexcept
{
    return window;
}

void Window::set_glfw_callbacks() const noexcept
{
    // Window resize callback function.
    glfwSetWindowSizeCallback(window, [](GLFWwindow* w, int width, int height) {
        auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
        ASSERT(win, "GLFW callback user data pointer has not been set.");
        ASSERT_FMT(width >= 0 && height >= 0,
                   "Invalid dimensions of window {}: {}, {}",
                   win->title,
                   width,
                   height);
        win->size_callback(width, height);
    });

    // Window close callback function.
    glfwSetWindowCloseCallback(window, [](GLFWwindow* w) {
        auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
        ASSERT(win, "GLFW callback user data pointer has not been set.");
        win->close_callback();
    });

    // Key callback function.
    glfwSetKeyCallback(
        window,
        [](GLFWwindow* w, int key, int /*scancode*/, int action, int /*mods*/) {
            auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
            ASSERT(win, "GLFW callback user data pointer has not been set.");
            win->key_callback(key, action);
        });

    // Key typed (character) callback function.
    glfwSetCharCallback(window, [](GLFWwindow* w, unsigned keycode) {
        auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
        ASSERT(win, "GLFW callback user data pointer has not been set.");
        win->char_callback(keycode);
    });

    // Mouse button callback function.
    glfwSetMouseButtonCallback(
        window, [](GLFWwindow* w, int button, int action, int /*mods*/) {
            auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
            ASSERT(win, "GLFW callback user data pointer has not been set.");
            win->mouse_button_callback(button, action);
        });

    // Mouse scroll callback function.
    glfwSetScrollCallback(
        window, [](GLFWwindow* w, double x_offset, double y_offset) {
            auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
            ASSERT(win, "GLFW callback user data pointer has not been set.");
            win->mouse_scroll_callback(x_offset, y_offset);
        });

    // Mouse cursor position callback function.
    glfwSetCursorPosCallback(
        window, [](GLFWwindow* w, double x_position, double y_position) {
            auto* win = static_cast<Window*>(glfwGetWindowUserPointer(w));
            ASSERT(win, "GLFW callback user data pointer has not been set.");
            win->mouse_move_callback(x_position, y_position);
        });
}

void Window::size_callback(unsigned new_width, unsigned new_height)
{
    width  = new_width;
    height = new_height;

    WindowResizeEvent event(new_width, new_height);
    event_handler(event);
}

void Window::close_callback()
{
    WindowCloseEvent event;
    event_handler(event);
}

void Window::key_callback(int key, int action)
{
    switch (action) {
        case GLFW_PRESS: {
            KeyPressedEvent press_event(key, 0);
            event_handler(press_event);
            break;
        }
        case GLFW_RELEASE: {
            KeyReleasedEvent release_event(key);
            event_handler(release_event);
            break;
        }
        case GLFW_REPEAT: {
            // Number of repeats is currently always set to one. In
            // the future, it might be worth counting number of key
            // repeats.
            KeyPressedEvent repeat_event(key, 1);
            event_handler(repeat_event);
            break;
        }
    }
}

void Window::char_callback(unsigned keycode)
{
    KeyTypedEvent event(keycode);
    event_handler(event);
}

void Window::mouse_button_callback(int button, int action)
{
    switch (action) {
        case GLFW_PRESS: {
            MouseButtonPressedEvent press_event(button);
            event_handler(press_event);
            break;
        }
        case GLFW_RELEASE: {
            MouseButtonReleasedEvent release_event(button);
            event_handler(release_event);
            break;
        }
    }
}

void Window::mouse_scroll_callback(double x_offset, double y_offset)
{
    MouseScrolledEvent event(x_offset, y_offset);
    event_handler(event);
}

void Window::mouse_move_callback(double x_position, double y_position)
{
    MouseMovedEvent event(x_position, y_position);
    event_handler(event);
}

namespace {
void error_callback(int error, const char* description) noexcept
{
    constexpr std::array ignore_error_codes = {65544};

    if (std::find(ignore_error_codes.begin(),
                  ignore_error_codes.end(),
                  error) == ignore_error_codes.end()) {
        Log::error("GLFW error ({}): {}", error, description);
        std::abort();
    }
}
}

std::pair<unsigned, unsigned> Window::dimensions() const noexcept
{
    return {width, height};
}

void Window::set_position(int x, int y) // TODO
{
    glfwSetWindowPos(window, x, y);
}
