#include "ResponseQueue.hpp"
#include "Assert.hpp"

void ResponseQueue::set_connection(
    std::shared_ptr<Connection> connection) noexcept
{
    this->connection = std::move(connection);
}

void ResponseQueue::register_write(std::string message)
{
    FMT_ASSERT(connection, "Set the connection before registering a write.");
    entries.push(std::move(message));
    if (!write_currently_registered) { register_write(); }
}

void ResponseQueue::register_write()
{
    write_currently_registered = true;
    connection->async_write(std::move(entries.front()),
                            [this](std::shared_ptr<Connection> /*connection*/) {
                                if (entries.empty()) {
                                    write_currently_registered = false;
                                } else {
                                    register_write();
                                }
                            });
    entries.pop(); // Consume one entry from the queue.
}
