#ifndef FILE_PATH_CONFIGURATION_HPP
#define FILE_PATH_CONFIGURATION_HPP

#include <filesystem>

#include <boost/program_options.hpp>

using VariablesMap       = boost::program_options::variables_map;
using OptionsDescription = boost::program_options::options_description;

class FilePathConfiguration {
  public:
    std::filesystem::path font;
    std::filesystem::path shader;
    int x, y;
    int delay;
    int tickrate;

  public:
    FilePathConfiguration(const VariablesMap& variables_map);
};

#endif
