#include "FilePathConfiguration.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>

FilePathConfiguration::FilePathConfiguration(const VariablesMap& variables_map)
{
    if (variables_map.count("font")) {
        font = variables_map["font"].as<std::filesystem::path>();
        fmt::print("Font path was set to {}\n", font);
    } else {
        fmt::print("Font path was not set.\n");
    }

    if (variables_map.count("shader")) {
        shader = variables_map["shader"].as<std::filesystem::path>();
        fmt::print("Shader path was set to {}\n", shader);
    } else {
        fmt::print("Shader was not set.\n");
    }

    if (variables_map.count("x")) {
        x = variables_map["x"].as<int>();
        fmt::print("x was set to {}\n", x);
    } else {
        fmt::print("x was not set.\n");
    }

    if (variables_map.count("y")) {
        y = variables_map["y"].as<int>();
        fmt::print("y was set to {}\n", y);
    } else {
        fmt::print("y was not set.\n");
    }

    if (variables_map.count("delay")) {
        delay = variables_map["delay"].as<int>();
        fmt::print("Delay was set to {}\n", delay);
    } else {
        fmt::print("Delay was not set.\n");
    }

    if (variables_map.count("tickrate")) {
        tickrate = variables_map["tickrate"].as<int>();
        fmt::print("Tickrate was set to {}\n", tickrate);
    } else {
    }
}
