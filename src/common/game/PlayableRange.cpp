#include "PlayableRange.hpp"

std::ostream& operator<<(std::ostream& os, const PlayableRange& p)
{
    os << "(" << p.min_x << ", " << p.max_x << ", " << p.min_y << ", "
       << p.max_y << ")";
    return os;
}
