#include "Random.hpp"

#include "game/Food.hpp"
#include "game/Game.hpp"

#include <algorithm>

namespace {
glm::vec4 get_color();
glm::vec2 get_position(PlayableRange playable_range, float radius);
}

bool FoodInstance::should_be_eaten(const Player& player) const noexcept
{
    return glm::distance(center, player.center) < player.radius;
}

std::ostream& operator<<(std::ostream& os, const FoodInstance& food_instance)

{
    os << "[" << food_instance.center << ", " << food_instance.color << "]\n";
    return os;
}

Food::Food(const PlayableRange& playable_range) noexcept
    : playable_range(playable_range)
{
    food_instances.resize(n_instances);

    // Initialize food instances with random positions and colors.
    for (auto& instance : food_instances) {
        instance.center = get_position(playable_range, radius);
        instance.color  = get_color();
    }
}

std::size_t Food::remove_eaten(const Player& player) noexcept
{
    auto it = std::remove_if(food_instances.begin(),
                             food_instances.end(),
                             [&player](const FoodInstance& instance) {
                                 return instance.should_be_eaten(player);
                             });
    food_instances.erase(it, food_instances.end());
    return n_instances - food_instances.size();
}

void Food::create_new_instances() noexcept
{
    std::size_t n_new = n_instances - food_instances.size();
    for (std::size_t i = 0; i < n_new; ++i) {
        food_instances.push_back(
            {get_position(playable_range, radius), get_color()});
    }
}

float Food::get_radius() const noexcept
{
    return radius;
}

namespace {
glm::vec4 get_color()
{
    static std::uniform_real_distribution<float> dist_c(0.3f, 1.0f);
    return {dist_c(Random::generator()),
            dist_c(Random::generator()),
            dist_c(Random::generator()),
            dist_c(Random::generator())};
}

glm::vec2 get_position(PlayableRange playable_range, float radius)
{
    auto [min_x, max_x, min_y, max_y] = playable_range;
    static std::uniform_real_distribution<float> dist_x(min_x + radius / 2.0f,
                                                        max_x - radius / 2.0f);
    static std::uniform_real_distribution<float> dist_y(min_y + radius / 2.0f,
                                                        max_y - radius / 2.0f);
    return {dist_x(Random::generator()), dist_y(Random::generator())};
}
}

std::vector<FoodInstance>::const_iterator Food::begin() const
{
    return food_instances.begin();
}

std::vector<FoodInstance>::const_iterator Food::end() const
{
    return food_instances.end();
}

std::ostream& operator<<(std::ostream& os, const Food& food)
{
    // os << "(";
    // for (std::size_t i = 0; i < food.food_instances.size(); ++i) {
    // os << food.food_instances[i];
    // if (i != food.food_instances.size() - 1) { os << ", "; }
    //}
    os << "["
       << "n instances: " << food.n_instances << ", radius: " << food.radius
       << ", playable range: " << food.playable_range << "]";
    return os;
}
