#ifndef GAME_HPP
#define GAME_HPP

#include "Timestep.hpp"
#include "game/Player.hpp"
#include "game/Food.hpp"
#include "game/PlayableRange.hpp"

#include <glm/glm.hpp>

/* Stores game data. */
class Game {
  public:
    struct State {
        State() = default;
        State(const std::vector<std::string>& player_names);

        glm::vec2 border_size = {2.0f, 2.0f};

        Food food;
        std::vector<Player> players;

        PlayableRange get_playable_range() const noexcept;
        friend std::ostream& operator<<(std::ostream& os, const State& food);
        void update_score() noexcept;
        Player& get_player(std::string name);
        const Player& get_player(std::string name) const;
    };

  public:
    constexpr static float increase_step = 0.0009;
    State state;

  public:
    Game(const std::vector<std::string>& player_names);
    Game(const State& state);

    void on_update_state();
    void update_player_position(std::string name,
                                Timestep timestep,
                                glm::vec2 movement_command);

  private:
    void update_food() noexcept;
    void update_player_size() noexcept;

    void move_player(glm::vec2 delta, Player& player) noexcept;
};

#endif
