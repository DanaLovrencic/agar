#ifndef PLAYABLE_RANGE_HPP
#define PLAYABLE_RANGE_HPP

#include <ostream>

struct PlayableRange {
    float min_x, max_x;
    float min_y, max_y;

    friend std::ostream& operator<<(std::ostream& os,
                                    const PlayableRange& p);
};

#endif
