#include "game/Game.hpp"
#include "Assert.hpp"

#include <algorithm>
#include <fmt/core.h>

namespace {
float clamp_to_range(float x, float min, float max) noexcept;
}

Game::State::State(const std::vector<std::string>& player_names)
    : food(get_playable_range())
{
    players.reserve(player_names.size());
    for (const auto& name : player_names) { players.emplace_back(name); }
}

PlayableRange Game::State::get_playable_range() const noexcept
{
    return {-border_size.x / 2.0f,
            border_size.x / 2.0f,
            -border_size.y / 2.0f,
            border_size.y / 2.0f};
}

std::ostream& operator<<(std::ostream& os, const Game::State& s)
{
    for (const auto& player : s.players) { os << player.center << " "; }
    return os;
}

Game::Game(const std::vector<std::string>& player_names) : state(player_names)
{}

Game::Game(const State& state) : state(state) {}

void Game::update_player_position(std::string name,
                                  Timestep timestep,
                                  glm::vec2 movement_command)
{
    auto& player    = state.get_player(name);
    glm::vec2 delta = player.speed * timestep * movement_command;
    move_player(delta, player);
}

void Game::on_update_state()
{
    update_player_size();
    update_food();
}

/* Delete old and create new food instances. */
void Game::update_food() noexcept
{
    for (std::size_t i = 0; i < state.players.size(); ++i) {
        auto& player   = state.players[i];
        player.n_eaten = state.food.remove_eaten(player);
        state.food.create_new_instances();
    }
}

/* Update player size and check if game over. */
void Game::update_player_size() noexcept
{
    // Check if some player has been eaten.
    for (auto& player : state.players) {
        for (auto& opponent : state.players) {
            // Check intersection.
            if (player.score > opponent.score &&
                glm::distance(player.center, opponent.center) < player.radius) {
                player.score += opponent.score; // Update score.
                player.radius += opponent.score * increase_step;
                opponent =
                    Player(opponent.name, opponent.color); // Reset opponent.
            }
        }
    }

    // Increase player size.
    for (std::size_t i = 0; i < state.players.size(); ++i) {
        auto& player = state.players[i];
        player.radius += player.n_eaten * increase_step;
        player.score += player.n_eaten; // Update score.
        player.n_eaten = 0;
    }
}

void Game::move_player(glm::vec2 delta, Player& player) noexcept
{
    auto [min_x, max_x, min_y, max_y] = state.get_playable_range();

    auto new_position = player.center + delta;

    player.center = {
        clamp_to_range(
            new_position.x, min_x + player.radius, max_x - player.radius),
        clamp_to_range(
            new_position.y, min_y + player.radius, max_y - player.radius)};
}

Player& Game::State::get_player(std::string name)
{
    auto it = std::find_if(
        players.begin(), players.end(), [&name](const Player& player) {
            return player.name == name;
        });

    ASSERT_FMT(it != players.end(), "Player with name {} should exist.", name);

    return *it;
}

const Player& Game::State::get_player(std::string name) const
{
    auto it = std::find_if(
        players.begin(), players.end(), [&name](const Player& player) {
            return player.name == name;
        });

    ASSERT_FMT(it != players.end(), "Player with name {} should exist.", name);

    return *it;
}

namespace {
float clamp_to_range(float x, float min, float max) noexcept
{
    return std::min(std::max(x, min), max);
}
}
