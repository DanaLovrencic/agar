#include "GameRenderer.hpp"

#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

GameRenderer::GameRenderer(const FilePathConfiguration& path_config,
                           const Game::State& state) noexcept
    : renderer(path_config), state(state)
{}

void GameRenderer::draw_border() noexcept
{
    renderer.draw_quad(
        {0.0f, 0.0f}, state.border_size, {0.4f, 0.9f, 0.6f, 1.0f}, false);
}

void GameRenderer::draw_player() noexcept
{
    for (const auto& player : state.players) {
        renderer.draw_circle(player.center, player.radius, player.color);
    }
}

void GameRenderer::draw_food() noexcept
{
    for (const auto& food_instance : state.food) {
        renderer.draw_circle(
            food_instance.center, state.food.get_radius(), food_instance.color);
    }
}

void GameRenderer::draw_scene(
    const OrthographicCameraController& camera_controller,
    const Window& window,
    const std::string& name) noexcept
{
    renderer.set_clear_color({0.9, 0.9, 0.9, 1.0f});
    renderer.clear();
    renderer.begin_scene(camera_controller.get_camera());

    renderer.begin_batch();
    draw_food();
    draw_player();
    renderer.end_batch();
    render_gui(window, name);
    // client.game_renderer.draw_border();
}

void GameRenderer::render_score(const Window& /*window*/,
                                const std::string& /*name*/) noexcept
{}

Renderer& GameRenderer::get_renderer()
{
    return renderer;
}
