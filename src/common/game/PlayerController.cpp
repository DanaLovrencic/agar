#include "PlayerController.hpp"
#include "Assert.hpp"

namespace {
float clamp_to_range(float x, float min, float max) noexcept;
}

void PlayerController::on_update(Timestep timestep,
                                 glm::vec2 movement_command) noexcept
{
    glm::vec2 delta = player.speed * timestep * movement_command;
    move_player(delta);
}

PlayerController::PlayerController(Player& player, PlayableRange playable_range)
    : player(player), playable_range(std::move(playable_range))
{}

// Change players position for shift delta.
void PlayerController::move_player(glm::vec2 delta) noexcept
{
    auto [min_x, max_x, min_y, max_y] = playable_range;
    auto new_position                 = player.center + delta;

    player.center = {clamp_to_range(new_position.x,
                                    min_x + player.radius / 2.0f,
                                    max_x - player.radius / 2.0f),
                     clamp_to_range(new_position.y,
                                    min_y + player.radius / 2.0f,
                                    max_y - player.radius / 2.0f)};
}

namespace {
float clamp_to_range(float x, float min, float max) noexcept
{
    return std::min(std::max(x, min), max);
}
}
