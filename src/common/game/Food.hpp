#ifndef FOOD_HPP
#define FOOD_HPP

#include <glm/glm.hpp>
#include <vector>
#include <tuple>

#include "game/Player.hpp"
#include "game/PlayableRange.hpp"

#include <boost/serialization/serialization.hpp>

class Food;
namespace boost::serialization {
template<class Archive>
void serialize(Archive& ar, Food& f, const unsigned int /*version*/);
}

class FoodInstance {
  public:
    glm::vec2 center;
    glm::vec4 color;

  public:
    bool should_be_eaten(const Player& player) const noexcept;

    friend std::ostream& operator<<(std::ostream& os,
                                    const FoodInstance& food_instance);
};

class Food {
  private:
    /* Total number of food instances on the screen after every update. */
    std::size_t n_instances = 50;
    /* All food instances share same radius size. */
    float radius = 0.02f;

    PlayableRange playable_range;
    std::vector<FoodInstance> food_instances;

  public:
    Food() = default;
    Food(const PlayableRange& playable_range) noexcept;

    /* Returns number of eaten food instances. */
    std::size_t remove_eaten(const Player& player) noexcept;
    void create_new_instances() noexcept;

    float get_radius() const noexcept;

    std::vector<FoodInstance>::const_iterator begin() const;
    std::vector<FoodInstance>::const_iterator end() const;

    friend std::ostream& operator<<(std::ostream& os, const Food& food);
    template<class Archive>
    friend void boost::serialization::serialize(Archive& ar,
                                                Food& f,
                                                const unsigned int /*version*/);
};

#endif
