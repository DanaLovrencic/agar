#ifndef ASSERT_HPP
#define ASSERT_HPP

#include "Log.hpp"
#include <cstdlib>

#ifndef NDEBUG
    #define ASSERT(expression, message)                       \
        do {                                                  \
            if (!(expression)) {                              \
                ::Log::error("[ASSERTION FAILED] ({}:{}) {}", \
                             __FILE__,                        \
                             __LINE__,                        \
                             message);                        \
                ::std::abort();                               \
            }                                                 \
        } while (false)

    #define ASSERT_FMT(expression, format_string, ...)                    \
        do {                                                              \
            if (!(expression)) {                                          \
                auto message = ::fmt::format(format_string, __VA_ARGS__); \
                ::Log::error("[ASSERTION FAILED] ({}:{}) {}",             \
                             __FILE__,                                    \
                             __LINE__,                                    \
                             message);                                    \
                ::std::abort();                                           \
            }                                                             \
        } while (false)

    #define ASSERT_UNREACHABLE(message)                                \
        do {                                                           \
            ::Log::error("[UNREACHABLE STATEMENT REACHED] ({}:{}) {}", \
                         __FILE__,                                     \
                         __LINE__,                                     \
                         message);                                     \
            ::std::abort();                                            \
        } while (false)

    #define ASSERT_UNREACHABLE_FMT(format_string, ...)                         \
        do {                                                                   \
            auto message = ::fmt::format(format_string, __VA_ARGS__);          \
            ::Log::error(                                                      \
                "[ASSERTION FAILED] ({}:{}) {}", __FILE__, __LINE__, message); \
            ::std::abort();                                                    \
        } while (false)

#else
    #define ASSERT(expression, message)
    #define ASSERT_FMT(expression, format_string, ...)
    #define ASSERT_UNREACHABLE(message)          ::std::abort()
    #define ASSERT_UNREACHABLE_FMT(message, ...) ::std::abort()
#endif

#endif
