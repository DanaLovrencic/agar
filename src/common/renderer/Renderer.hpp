#ifndef RENDERER_RENDERER_HPP
#define RENDERER_RENDERER_HPP

#include "renderer/OrthographicCamera.hpp"
#include "renderer/Shader.hpp"
#include "renderer/VertexArray.hpp"
#include "FilePathConfiguration.hpp"

class Renderer {
  private:
    struct Vertex {
        glm::vec3 position;
        glm::vec3 center;
        float radius;
        glm::vec4 color;
    };

    struct BatchData {
        std::vector<Vertex> quad_buffer;
        std::vector<Vertex>::iterator quad_buffer_it;
        std::size_t index_count = 0;
    };

  private:
    constexpr static std::size_t max_quad_count   = 1000;
    constexpr static std::size_t max_vertex_count = max_quad_count * 4;
    constexpr static std::size_t max_index_count  = max_quad_count * 6;

    BatchData batch_data;

    ShaderLibrary shader_library;
    VertexArray vertex_array;

  public:
    Renderer(const FilePathConfiguration& path_config);

    void on_window_resize(unsigned width, unsigned height) const noexcept;

    void begin_scene(const OrthographicCamera& camera) noexcept;

    void draw_quad(const glm::vec2& position,
                   const glm::vec2& size,
                   const glm::vec4& color,
                   bool fill = true) noexcept;

    void draw_quad(const glm::vec3& position,
                   const glm::vec2& size,
                   const glm::vec4& color,
                   bool fill = true) noexcept;

    void draw_circle(const glm::vec2& center,
                     float radius,
                     const glm::vec4& color) noexcept;

    void draw_circle(const glm::vec3& center,
                     float radius,
                     const glm::vec4& color) noexcept;

    void set_clear_color(const glm::vec4& color) const noexcept;
    void clear() const noexcept;

    void begin_batch() noexcept;
    void end_batch() noexcept;

  private:
    void initialize_shader_library(const FilePathConfiguration& path_config);
    void initialize_batch_renderer() noexcept;
    void flush() noexcept;
};

#endif
