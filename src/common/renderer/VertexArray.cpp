#include "renderer/VertexArray.hpp"

#include "Assert.hpp"

#include <GL/glew.h>

namespace {
GLenum shader_data_type_to_opengl_base_type(ShaderDataType type) noexcept;
}

VertexArray::VertexArray() noexcept
{
    glCreateVertexArrays(1, &renderer_id);
}

VertexArray::~VertexArray() noexcept
{
    if (renderer_id) { glDeleteVertexArrays(1, &renderer_id); }
}

VertexArray::VertexArray(VertexArray&& other) noexcept
    : renderer_id(other.renderer_id),
      vertex_buffers(std::move(other.vertex_buffers)),
      index_buffer(std::move(other.index_buffer))
{
    other.renderer_id = 0;
}

VertexArray& VertexArray::operator=(VertexArray&& other) noexcept
{
    if (renderer_id != 0) { glDeleteVertexArrays(1, &renderer_id); }

    renderer_id    = other.renderer_id;
    vertex_buffers = std::move(other.vertex_buffers);
    index_buffer   = std::move(other.index_buffer);

    other.renderer_id = 0;

    return *this;
}

void VertexArray::bind() const noexcept
{
    glBindVertexArray(renderer_id);
}

void VertexArray::unbind() const noexcept
{
    glBindVertexArray(0);
}

void VertexArray::add_vertex_buffer(VertexBuffer vertex_buffer) noexcept
{
    // Make sure layout has been set for the vertex buffer since layout is
    // remembered inside vertex array object.
    ASSERT(vertex_buffer.get_layout().get_elements().size(),
           "Provided vertex buffer has no layout.");

    bind();
    vertex_buffer.bind();

    unsigned index     = 0;
    const auto& layout = vertex_buffer.get_layout();
    for (const auto& element : layout) {
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(
            index, // Index of the vertex buffer element.
            element.get_component_count(), // Count of components.
            shader_data_type_to_opengl_base_type(element.type), // Type.
            element.normalized ? GL_TRUE : GL_FALSE, // Normalized flag.
            layout.get_stride(), // Size of each vertex (stride).
            reinterpret_cast<const void*>(element.offset)); // Offset.
        index++;
    }

    vertex_buffers.push_back(std::move(vertex_buffer));
}

void VertexArray::set_index_buffer(IndexBuffer index_buffer) noexcept
{
    // Bind vertex array first and then bind the index buffer so the index
    // buffer becomes associated with that vertex array.
    bind();
    index_buffer.bind();

    this->index_buffer = std::move(index_buffer);
}

const IndexBuffer& VertexArray::get_index_buffer() const noexcept
{
    ASSERT(index_buffer, "Vertex array index buffer has not yet been set.");
    return *index_buffer;
}

const std::vector<VertexBuffer>&
VertexArray::get_vertex_buffers() const noexcept
{
    return vertex_buffers;
}

namespace {
GLenum shader_data_type_to_opengl_base_type(ShaderDataType type) noexcept
{
    switch (type) {
        case ShaderDataType::Float: return GL_FLOAT;
        case ShaderDataType::Float2: return GL_FLOAT;
        case ShaderDataType::Float3: return GL_FLOAT;
        case ShaderDataType::Float4: return GL_FLOAT;
        case ShaderDataType::Mat3: return GL_FLOAT;
        case ShaderDataType::Mat4: return GL_FLOAT;
        case ShaderDataType::Int: return GL_INT;
        case ShaderDataType::Int2: return GL_INT;
        case ShaderDataType::Int3: return GL_INT;
        case ShaderDataType::Int4: return GL_INT;
        case ShaderDataType::Bool: return GL_BOOL;
        case ShaderDataType::None:
            ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    ASSERT_UNREACHABLE("Unhandled shader data type.");
}
}
