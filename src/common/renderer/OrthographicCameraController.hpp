#ifndef RENDERER_ORTHOGRAPHICCAMERACONTROLLER_HPP
#define RENDERER_ORTHOGRAPHICCAMERACONTROLLER_HPP

#include "events/ApplicationEvent.hpp"
#include "events/Event.hpp"
#include "events/MouseEvent.hpp"
#include "renderer/OrthographicCamera.hpp"
#include "Timestep.hpp"
#include "Input.hpp"

class OrthographicCameraController {
  private:
    const Input& input;
    float aspect_ratio;
    float zoom_level = 1.0f;
    OrthographicCamera camera;
    bool rotation_enabled;

    glm::vec3 camera_position = {0.0f, 0.0f, 0.0f};
    float camera_rotation     = 0.0f;

  public:
    /* Creates a camera with a zoom level equal to one meaning that there are
     * two units of space vertically. Horizontal space is calculated based on
     * the aspect ratio. */
    OrthographicCameraController(const Input& input,
                                 float aspect_ratio,
                                 bool rotation_enabled = false) noexcept;

    void on_update(Timestep timestep) noexcept;
    void on_event(Event& event) noexcept;
    void set_position(glm::vec2 position);

    const OrthographicCamera& get_camera() const noexcept;

  private:
    bool on_mouse_scroll(MouseScrolledEvent& event) noexcept;
    bool on_window_resize(WindowResizeEvent& event) noexcept;
};

#endif
