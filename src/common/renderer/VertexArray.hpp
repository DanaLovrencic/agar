#ifndef RENDERER_VERTEXARRAY_HPP
#define RENDERER_VERTEXARRAY_HPP

#include "renderer/Buffer.hpp"

#include <optional>

class VertexArray {
  private:
    unsigned renderer_id;
    std::vector<VertexBuffer> vertex_buffers;
    std::optional<IndexBuffer> index_buffer;

  public:
    VertexArray() noexcept;
    ~VertexArray() noexcept;

    VertexArray(const VertexArray& other) = delete;
    VertexArray& operator=(const VertexArray& other) = delete;

    VertexArray(VertexArray&& other) noexcept;
    VertexArray& operator=(VertexArray&& other) noexcept;

    void bind() const noexcept;
    void unbind() const noexcept;

    void add_vertex_buffer(VertexBuffer vertex_buffer) noexcept;
    void set_index_buffer(IndexBuffer index_buffer) noexcept;

    const std::vector<VertexBuffer>& get_vertex_buffers() const noexcept;
    const IndexBuffer& get_index_buffer() const noexcept;
};

#endif
