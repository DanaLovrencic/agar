#include "Shader.hpp"

#include <fstream>

#include "Log.hpp"
#include "Assert.hpp"

#include <GL/glew.h>

namespace {

enum class ShaderType : unsigned {
    Vertex   = GL_VERTEX_SHADER,
    Fragment = GL_FRAGMENT_SHADER
};

std::string shader_type_to_string(ShaderType type) noexcept;

class Shader {
  private:
    unsigned shader_id;
    std::string code;
    ShaderType type;

  public:
    Shader(ShaderType type, const std::filesystem::path& filename);
    ~Shader() noexcept;

    Shader(const Shader& other) = delete;
    Shader& operator=(const Shader& other) = delete;

    [[maybe_unused]] Shader(Shader&& other) noexcept;
    [[maybe_unused]] Shader& operator=(Shader&& other) noexcept;

    void compile();
    void attach(unsigned program_id) const noexcept;
    void detach(unsigned program_id) const noexcept;
};

}

ShaderProgram::ShaderProgram(
    const std::filesystem::path& vertex_shader_filename,
    const std::filesystem::path& fragment_shader_filename)
    : program_id(glCreateProgram())
{
    // Load, compile and attach vertex shader.
    Shader vertex(ShaderType::Vertex, vertex_shader_filename);
    vertex.compile();
    vertex.attach(program_id); // Attach vertex shader to shader program.

    // Load, compile and attach fragment shader.
    Shader fragment(ShaderType::Fragment, fragment_shader_filename);
    fragment.compile();
    fragment.attach(program_id); // Attach fragment shader to shader program.

    // Link and validate.
    glLinkProgram(program_id);
    glValidateProgram(program_id);
    int validation_result;
    glGetProgramiv(program_id, GL_VALIDATE_STATUS, &validation_result);
    if (validation_result == GL_FALSE) {
        throw ShaderError("Failed to validate linked shader program.");
    }

    // Detach used shaders.
    fragment.detach(program_id);
    vertex.detach(program_id);
}

ShaderProgram::~ShaderProgram() noexcept
{
    if (program_id) { glDeleteProgram(program_id); }
}

ShaderProgram::ShaderProgram(ShaderProgram&& other) noexcept
    : program_id(other.program_id),
      uniform_location_cache(std::move(other.uniform_location_cache))
{
    other.program_id = 0;
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram&& other) noexcept
{
    if (program_id) { glDeleteProgram(program_id); }

    program_id             = other.program_id;
    uniform_location_cache = std::move(other.uniform_location_cache);

    other.program_id = 0;

    return *this;
}

void ShaderProgram::bind() const noexcept
{
    glUseProgram(program_id);
}

void ShaderProgram::unbind() const noexcept
{
    glUseProgram(0);
}

void ShaderProgram::set_uniform4f(std::string_view name,
                                  const glm::vec4& value) noexcept
{
    glUniform4f(get_uniform_location(name), value.x, value.y, value.z, value.w);
}

void ShaderProgram::set_uniform1i(std::string_view name, int value) noexcept
{
    glUniform1i(get_uniform_location(name), value);
}

void ShaderProgram::set_uniform_mat4(std::string_view name,
                                     const glm::mat4& value) noexcept
{
    glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, &value[0][0]);
}

int ShaderProgram::get_uniform_location(std::string_view name) noexcept
{
    auto it = std::find_if(
        uniform_location_cache.begin(),
        uniform_location_cache.end(),
        [name](const auto& entry) noexcept { return entry.first == name; });
    if (it != uniform_location_cache.end()) { return it->second; }

    std::string name_str(name);
    int location = glGetUniformLocation(program_id, name_str.c_str());
    if (location == -1) {
        Log::warn("Uniform {} does not exist or is not used in the shader.",
                  name_str);
    }

    uniform_location_cache[name_str] = location;
    return location;
}

ShaderError::ShaderError(const std::string& message) noexcept
    : std::runtime_error(message)
{}

ShaderError::ShaderError(const char* message) noexcept
    : std::runtime_error(message)
{}

void ShaderLibrary::add(std::string name, ShaderProgram shader) noexcept
{
    [[maybe_unused]] auto [it, inserted] =
        shaders.insert({std::move(name), std::move(shader)});
    ASSERT_FMT(inserted,
               "Shader named {} already exists the shader library.",
               it->first);
}

ShaderProgram& ShaderLibrary::get(const std::string& name) noexcept
{
    ASSERT_FMT(
        shaders.count(name), "No shader named {} in the shader library.", name);
    return shaders.at(name);
}

namespace {

std::string shader_type_to_string(ShaderType type) noexcept
{
    switch (type) {
        case ShaderType::Vertex: return "vertex";
        case ShaderType::Fragment: return "fragment";
    }
    ASSERT_UNREACHABLE("Unhandled shader type.");
}

Shader::Shader(ShaderType type, const std::filesystem::path& filename)
    : type(type)
{
    std::ifstream ifs(filename, std::ios::in);
    if (!ifs.is_open()) {
        throw ShaderError(
            fmt::format("Failed to open shader file: {}", filename.c_str()));
    }
    std::stringstream sstr;
    sstr << ifs.rdbuf();
    code = sstr.str();
}

Shader::~Shader() noexcept
{
    if (shader_id) { glDeleteShader(shader_id); }
}

Shader::Shader(Shader&& other) noexcept
    : shader_id(other.shader_id), code(std::move(other.code)), type(other.type)
{
    other.shader_id = 0;
}

Shader& Shader::operator=(Shader&& other) noexcept
{
    if (shader_id) { glDeleteShader(shader_id); }

    shader_id = other.shader_id;
    code      = std::move(other.code);
    type      = other.type;

    other.shader_id = 0;

    return *this;
}

void Shader::compile()
{
    shader_id            = glCreateShader(static_cast<unsigned>(type));
    const char* code_ptr = code.c_str();
    glShaderSource(shader_id, 1, &code_ptr, nullptr);
    glCompileShader(shader_id);

    // Error handling.
    int result;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) { // Shader did not compile properly.
        int message_length;   // Error message length.
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &message_length);
        std::string message(message_length + 1, '\0'); // Reserve message space.
        glGetShaderInfoLog(shader_id,
                           message_length,
                           &message_length,
                           const_cast<char*>(message.c_str()));
        glDeleteShader(shader_id);
        throw ShaderError(fmt::format("Failed to compile {} shader! {}",
                                      shader_type_to_string(type),
                                      message));
    }
}

void Shader::attach(unsigned program_id) const noexcept
{
    glAttachShader(program_id, shader_id);
}

void Shader::detach(unsigned program_id) const noexcept
{
    glDetachShader(program_id, shader_id);
}

}
