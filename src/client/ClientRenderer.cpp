#include "ClientRenderer.hpp"

#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

ClientRenderer::ClientRenderer(const FilePathConfiguration& path_config,
                               const Game::State& state) noexcept
    : GameRenderer(path_config, state)
{}

void ClientRenderer::render_gui(const Window& window,
                                const std::string& name) noexcept
{
    // IMGUI ---------------------------------------- BEGIN
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    // IMGUI =========================================
    static constexpr float distance_h = 560.0f;
    static constexpr float distance_w = 10.0f;

    ImGui::SetNextWindowPos({distance_w, distance_h}, ImGuiCond_Always);
    ImGui::Begin("Options",
                 nullptr,
                 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration |
                     ImGuiWindowFlags_AlwaysAutoResize |
                     ImGuiWindowFlags_NoBackground);
    ImGui::Text("%s", state.get_player(name).name.data());
    ImGui::SameLine();
    ImGui::Text("%d", state.get_player(name).score);
    ImGui::SameLine();
    ImGui::Indent(80.0f);
    ImGui::Checkbox("Mouse movement", &mouse_movement);

    ImGui::SameLine();
    ImGui::Checkbox("Prediction", &prediction);
    ImGui::End();

    auto [width, height] = window.dimensions();
    auto name_len        = 2 * width / 37.0f;
    auto name_h          = 2 * height / 37.0f;

    auto wp = state.get_player(name).get_window_relative_center(
        {window.dimensions().first - name_len,
         window.dimensions().second - name_h});

    ImGui::SetNextWindowPos({wp.x, wp.y}, ImGuiCond_Always);
    ImGui::Begin("Name",
                 nullptr,
                 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration |
                     ImGuiWindowFlags_AlwaysAutoResize |
                     ImGuiWindowFlags_NoSavedSettings |
                     ImGuiWindowFlags_NoFocusOnAppearing |
                     ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoBackground);
    ImGui::Text("%s", state.get_player(name).name.data());
    ImGui::End();

    // IMGUI ---------------------------------------- END
    // Set display size.
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize =
        ImVec2(window.dimensions().first, window.dimensions().second);

    // Render ImGui to the screen.
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    // IMGUI =========================================
}

bool ClientRenderer::prediction_enabled() const noexcept
{
    return prediction;
}

bool ClientRenderer::mouse_movement_enabled() const noexcept
{
    return mouse_movement;
}
