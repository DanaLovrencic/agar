#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "ResponseQueue.hpp"
#include "Input.hpp"
#include "Configuration.hpp"
#include "Connection.hpp"
#include "game/GameRenderer.hpp"
#include "ClientRenderer.hpp"
#include "renderer/OrthographicCameraController.hpp"

class Client {
  private:
    using IOContext = boost::asio::io_context;
    using Acceptor  = boost::asio::ip::tcp::acceptor;
    using Endpoint  = boost::asio::ip::tcp::endpoint;
    using ErrorCode = boost::system::error_code;
    using Timer     = boost::asio::steady_timer;

    using MovementCommand = glm::vec2;
    bool show             = true;

  private:
    /* Communication data. */
    static constexpr std::string_view ip_address = "127.0.0.1";
    static constexpr unsigned short port         = 8080;

    ResponseQueue response_queue; // Handles multiple writings on the socket.
    IOContext io_context;
    std::shared_ptr<Connection> connection; // Connection to the server.

    Timer poll_inputs_timer;

    /* Game state data. */
    std::unique_ptr<Game> game;
    bool running = true;

    /* Render data. */
    Configuration configuration;
    Window window;
    Input input;
    std::unique_ptr<ClientRenderer> game_renderer;
    OrthographicCameraController camera_controller;

  public:
    Client(Window window, Configuration configuration);

    void async_run(); // Async game loop.

  private:
    void game_step();

    /* Action exectuted after async read operation. */
    void on_read_game_state(Game::State received_state);

    /* Synchronously exchanges data required for successful game start (name,
     * initial game state). */
    void initialize_game();

    /* Connects to the given ip_address and port. */
    std::shared_ptr<Connection> connect(IOContext& io_context,
                                        std::string_view ip_address,
                                        unsigned short port);

    /* Handles common window events (resizing and closing). */
    void on_event(Event& event) noexcept;

    void on_timer_expired();

    // Normalizes coordinates to range [-1, 1].
    glm::vec2 normalize(MovementCommand movement_command) const;
    // Moves origin of the coordinate system to the window center.
    glm::vec2 translate_to_center(MovementCommand movement_command) const;
    // Retrives currently active movement command.
    glm::vec2 get_movement_command() const;

    void send_movement_command(MovementCommand movement_command);
    void predict_new_state(MovementCommand movement_command);
    void render_scene();

    MovementCommand get_keyboard_movement_command() const;
};

#endif
