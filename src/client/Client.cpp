#include "Client.hpp"
#include "Serialization.hpp"
#include "Random.hpp"

#include <boost/program_options.hpp>
#include <fmt/ostream.h>

#include <thread>
#include <filesystem>
#include "ClientRenderer.hpp"

namespace {
std::optional<Configuration> parse_program_options(int argc, char** argv);
}

Client::Client(Window window, Configuration configuration)
    : poll_inputs_timer(io_context.get_executor()),
      configuration(std::move(configuration)),
      window(std::move(window)),
      input(this->window),
      camera_controller(input,
                        static_cast<float>(this->window.get_width()) /
                            this->window.get_height(),
                        true)
{
    // Initialize window.
    this->window.set_event_handler([this](Event& event) { on_event(event); });
    this->window.set_position(configuration.path_config.x,
                              configuration.path_config.y);
    window.set_vsync(false);
}

void Client::async_run()
{
    connection = connect(io_context, ip_address, port); // Connect to the server
    response_queue.set_connection(connection); // Initialize queue for writing.

    // Exchange data (initial state, player name) for successful game start.
    initialize_game();

    // Register next game state reception.
    connection->async_read([this](std::shared_ptr<Connection> /*connection*/,
                                  std::string message) {
        on_read_game_state(Serialization::deserialize<Game::State>(message));
    });

    // Register next game step.
    boost::asio::post(io_context, [this]() { game_step(); });

    // Register polling next movement command....
    poll_inputs_timer.expires_after(
        std::chrono::milliseconds(1000 / configuration.polling_rate));
    poll_inputs_timer.async_wait([this](const ErrorCode& error) {
        if (error) { throw std::runtime_error(error.message()); }

        // Register new timer and execute operation.
        on_timer_expired();
    });

    io_context.run(); // Run event processing loop.
}

void Client::game_step()
{
    window.poll_events();
    render_scene();

    // Register next game step operation.
    boost::asio::post(io_context, [this]() { game_step(); });
}

void Client::on_read_game_state(Game::State received_state)
{
    game->state = std::move(received_state); // Store new game state.

    // Register receive of next game state.
    connection->async_read([this](std::shared_ptr<Connection> /*connection*/,
                                  std::string message) {
        on_read_game_state(Serialization::deserialize<Game::State>(message));
    });
}

void Client::initialize_game()
{
    // Send player name.
    connection->write(configuration.player_name);

    // Get initial state.
    auto state    = Serialization::deserialize<Game::State>(connection->read());
    game          = std::make_unique<Game>(state);
    game_renderer = std::make_unique<ClientRenderer>(configuration.path_config,
                                                     game->state);
}

std::shared_ptr<Connection> Client::connect(IOContext& io_context,
                                            std::string_view ip_address,
                                            unsigned short port)
{
    auto connection = std::make_shared<Connection>(io_context.get_executor());
    connection->get_socket().connect(
        Endpoint(boost::asio::ip::make_address(ip_address), port));

    return connection;
}

void Client::on_event(Event& event) noexcept
{
    EventDispatcher dispatcher(event);
    dispatcher.dispatch<WindowCloseEvent>(
        [this](WindowCloseEvent& /*event*/) noexcept {
            running = false;
            return true; // Event has been handled.
        });

    dispatcher.dispatch<WindowResizeEvent>(
        [this](WindowResizeEvent& event) noexcept {
            game_renderer->get_renderer().on_window_resize(event.get_width(),
                                                           event.get_height());
            return false; // Event not handled (may be dispached elsewhere).
        });

    camera_controller.on_event(event);
}

void Client::on_timer_expired()
{
    auto movement_command = get_movement_command();
    send_movement_command(movement_command); // Async operation.
    if (game_renderer->prediction_enabled()) {
        predict_new_state(movement_command);
    }

    poll_inputs_timer.expires_after(
        std::chrono::milliseconds(1000 / configuration.polling_rate));
    poll_inputs_timer.async_wait([this](const ErrorCode& error) {
        if (error) { throw std::runtime_error(error.message()); }
        on_timer_expired();
        // Register timer.
    });
}

glm::vec2 Client::get_movement_command() const
{
    if (game_renderer->mouse_movement_enabled()) {
        auto [x_m, y_m] = input.get_mouse_position(); // (0, 0) -> LU corner
        // (0, 0) -> Center of the window, (+/-1, +/-1) -> Corners
        return normalize(translate_to_center({x_m, y_m}));
    } else {
        auto movement_command = get_keyboard_movement_command();
        return movement_command;
    }
}

glm::vec2 Client::translate_to_center(glm::vec2 position) const
{
    return {position.x - window.get_width() / 2.0f,
            -position.y + window.get_height() / 2.0f};
}

glm::vec2 Client::normalize(glm::vec2 position) const
{
    return {position.x / (window.get_width() / 2.0f),
            position.y / (window.get_height() / 2.0f)};
}

void Client::send_movement_command(MovementCommand movement_command)
{
    // Send mouse position to the server with '60' (simulates lag).
    auto timer = std::make_unique<Timer>(io_context.get_executor());
    timer->expires_after(
        std::chrono::milliseconds(configuration.path_config.delay));
    timer->async_wait([this, movement_command, timer = std::move(timer)](
                          const ErrorCode& error) {
        if (error) { throw std::runtime_error(error.message()); }
        response_queue.register_write(
            Serialization::serialize(movement_command));
    });
}

void Client::predict_new_state(MovementCommand movement_command)
{
    game->update_player_position(
        configuration.player_name,
        Timestep(std::chrono::milliseconds(1000 / configuration.polling_rate)),
        movement_command);
}

void Client::render_scene()
{
    game_renderer->draw_scene(
        camera_controller, window, configuration.player_name);
    window.on_update();
}

Client::MovementCommand Client::get_keyboard_movement_command() const
{
    static constexpr float factor = 1.0;
    glm::vec2 movement_command    = {0.0, 0.0};
    switch (configuration.keyboard_option) {
        case KeyboardOption::Arrows:
            if (input.is_pressed(Key::Left)) {
                movement_command.x = -factor;
            } else if (input.is_pressed(Key::Right)) {
                movement_command.x = factor;
            } else if (input.is_pressed(Key::Down)) {
                movement_command.y = -factor;
            } else if (input.is_pressed(Key::Up)) {
                movement_command.y = factor;
            }
            break;
        case KeyboardOption::Letters:
            if (configuration.keyboard_option == KeyboardOption::Arrows) {
                if (input.is_pressed(Key::A)) {
                    movement_command.x = -factor;
                } else if (input.is_pressed(Key::D)) {
                    movement_command.x = factor;
                } else if (input.is_pressed(Key::S)) {
                    movement_command.y = -factor;
                } else if (input.is_pressed(Key::W)) {
                    movement_command.y = factor;
                }
            }
            break;
    }
    return movement_command;
}

namespace {
std::optional<Configuration> parse_program_options(int argc, char** argv)
{
    // Declare the supported options.
    OptionsDescription desc("Allowed options");
    namespace po = boost::program_options;
    // clang-format off
    desc.add_options()
        ("help,h", "produce help message")
        ("font,f", po::value<std::filesystem::path>(), "set font path")
        ("name,n", po::value<std::string>(), "set player name")
        ("shader,s", po::value<std::filesystem::path>(), "set shader path")
        ("x,x", po::value<int>(), "set windows position x")
        ("y,y", po::value<int>(), "set windows position y")
        ("delay,d", po::value<int>(), "set delay")
        ("polling_rate,p", po::value<int>(), "set polling rate")
        ("keyboard_option,k", po::value<std::string>(), "set keyboard option");
    // clang-format on

    // Store options and their values in the map.
    VariablesMap variables_map;
    po::store(po::parse_command_line(argc, argv, desc), variables_map);

    po::notify(variables_map);

    if (variables_map.count("help")) {
        fmt::print("{}\n", desc);
        return std::nullopt;
    }

    return Configuration(variables_map);
}
}

int main(int argc, char** argv)
{
    try {
        auto configuration = parse_program_options(argc, argv);
        if (!configuration) { return 0; } // Help option.

        Window window(configuration->player_name,
                      600,
                      600,
                      configuration->path_config.font);
        Client client(std::move(window), *configuration);

        client.async_run();
    } catch (const std::exception& exc) {}
}
