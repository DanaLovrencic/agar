#ifndef CLIENT_RENDERER_HPP
#define CLIENT_RENDERER_HPP

#include "FilePathConfiguration.hpp"
#include "game/Game.hpp"
#include "game/GameRenderer.hpp"

class ClientRenderer : public GameRenderer {
  private:
    bool prediction     = false;
    bool mouse_movement = false;

  public:
    ClientRenderer(const FilePathConfiguration& path_config,
                   const Game::State& state) noexcept;

    void render_gui(const Window& window,
                    const std::string& name) noexcept override;

    bool prediction_enabled() const noexcept;
    bool mouse_movement_enabled() const noexcept;
};

#endif
