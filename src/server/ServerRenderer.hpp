#ifndef CLIENT_RENDERER_HPP
#define CLIENT_RENDERER_HPP

#include "FilePathConfiguration.hpp"
#include "game/Game.hpp"
#include "game/GameRenderer.hpp"

#include "imgui.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

class ServerRenderer : public GameRenderer {
  private:
    int updates_per_second = 30;

  public:
    ServerRenderer(const FilePathConfiguration& path_config,
                   const Game::State& state) noexcept;

    void render_gui(const Window& window,
                    const std::string& name) noexcept override;
};

#endif
