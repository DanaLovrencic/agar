#include "Server.hpp"
#include "Timestep.hpp"
#include "Serialization.hpp"
#include "events/ApplicationEvent.hpp"
#include "Assert.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <thread>

namespace {
std::optional<FilePathConfiguration> parse_program_options(int argc,
                                                           char** argv);
}

Server::Server(Window window, const FilePathConfiguration& path_config)
    : update_timer(io_context.get_executor()),
      path_config(std::move(path_config)),
      window(std::move(window)),
      input(this->window),
      camera_controller(input,
                        static_cast<float>(this->window.get_width()) /
                            this->window.get_height(),
                        true)
{
    this->window.set_event_handler([this](Event& event) { on_event(event); });
    this->window.set_position(path_config.x, path_config.y);

    accept(io_context, n_players); // Waits for all players to join.

    auto player_names = get_player_names();

    game          = std::make_unique<Game>(player_names);
    game_renderer = std::make_unique<ServerRenderer>(path_config, game->state);

    send_initial_game_state();
    initalize_movement_commands(player_names);
    initalize_response_queues();
}

void Server::async_run()
{
    /* Register receiving movement commands from all clients. */
    register_async_reads();

    game_renderer->draw_scene(camera_controller, window);
    update_game_state(std::chrono::milliseconds(1000 / path_config.tickrate));

    io_context.run();
}

void Server::game_step()
{
    if (!running) { stop_game(); }

    update_player_positions();

    // Check if some player has been eaten, update player size, create new
    // food instances etc.
    game->on_update_state();

    // Send updated game state to all clients.
    send_game_state();

    window.poll_events();
    game_renderer->draw_scene(camera_controller, window);
    window.on_update();
}

void Server::on_read_movement_command(MovementCommand movement_command,
                                      std::string name)
{
    movement_commands[name].push_back(movement_command);

    // Register new read operation.
    connections[name]->async_read(
        [this, name = name](std::shared_ptr<Connection> /*connection*/,
                            std::string message) {
            on_read_movement_command(
                Serialization::deserialize<MovementCommand>(message), name);
        });
}

void Server::on_timer_expired()
{
    game_step();

    update_timer.expires_after(
        std::chrono::milliseconds(1000 / path_config.tickrate));
    update_timer.async_wait([this](const ErrorCode& error) {
        if (error) { throw std::runtime_error(error.message()); }
        on_timer_expired();
    });
}

void Server::on_event(Event& event) noexcept
{
    EventDispatcher dispatcher(event);
    dispatcher.dispatch<WindowCloseEvent>(
        [this](WindowCloseEvent& /*event*/) noexcept {
            running = false;
            return true; // Event has been handled.
        });

    dispatcher.dispatch<WindowResizeEvent>(
        [this](WindowResizeEvent& event) noexcept {
            game_renderer->get_renderer().on_window_resize(event.get_width(),
                                                           event.get_height());
            return false; // Event not handled (may be dispached elsewhere).
        });

    camera_controller.on_event(event);
}

void Server::register_async_reads()
{
    for (const auto& [name, connection] : connections) {
        connection->async_read(
            [this, name = name](std::shared_ptr<Connection> /*connection*/,
                                std::string message) {
                on_read_movement_command(
                    Serialization::deserialize<MovementCommand>(message), name);
            });
    }
}

void Server::accept(IOContext& io_context, std::size_t n_players)
{
    Acceptor acceptor(io_context.get_executor(),
                      Endpoint(boost::asio::ip::tcp::v4(), port));
    for (std::size_t i = 0; i < n_players; ++i) {
        auto connection =
            std::make_shared<Connection>(io_context.get_executor());
        acceptor.accept(connection->get_socket());

        auto name = connection->read(); // Read player name.

        ASSERT(!name.empty(), "Received name should not be empty.");

        auto [it, is_inserted] =
            connections.insert({std::move(name), connection});
        if (!is_inserted) {
            throw std::runtime_error(fmt::format(
                "Player with name {} already connected.", it->first));
        }
        fmt::print("Player '{}' connected!\n", it->first);
    }
}

void Server::update_game_state(std::chrono::milliseconds time)
{
    update_timer.expires_after(time);
    update_timer.async_wait([this](const ErrorCode& error) {
        if (error) { throw std::runtime_error(error.message()); }
        on_timer_expired();
    });
}

void Server::send_game_state()
{
    auto serialized_game_state = Serialization::serialize(game->state);
    for (const auto& [name, connection] : connections) {
        auto timer = std::make_unique<Timer>(io_context.get_executor());
        timer->expires_after(std::chrono::milliseconds(path_config.delay));
        timer->async_wait([this,
                           name = name,
                           serialized_game_state,
                           timer = std::move(timer)](const ErrorCode& error) {
            if (error) { throw std::runtime_error(error.message()); }
            response_queues[name].register_write(serialized_game_state);
        });
    }
}

void Server::update_player_positions()
{
    for (auto& [name, movement_command] : movement_commands) {
        // fmt::print("N commands: {}\n", movement_command.size());
        std::size_t i = 0;
        while (i < movement_command.size()) {
            game->update_player_position(name,
                                         Timestep(std::chrono::milliseconds(
                                             1000 / path_config.tickrate)),
                                         movement_command[i++]);
            i++;
        }
        movement_command.clear();
    }
}

std::vector<std::string> Server::get_player_names()
{
    std::vector<std::string> player_names;
    player_names.reserve(n_players);
    for (const auto& [name, connection] : connections) {
        player_names.push_back(name);
    }

    return player_names;
}

void Server::send_initial_game_state()
{
    for (const auto& [name, connection] : connections) {
        connection->write(Serialization::serialize(game->state));
    }
}

void Server::initalize_movement_commands(
    const std::vector<std::string>& player_names)
{
    for (const auto& name : player_names) {
        movement_commands[name].push_back({0.0f, 0.0f});
    }
}

void Server::initalize_response_queues()
{
    for (const auto& [name, connection] : connections) {
        response_queues[name].set_connection(connection);
    }
}

void Server::stop_game()
{
    for (auto& connection : connections) {
        connection.second->get_socket().close();
    }
}

namespace {
std::optional<FilePathConfiguration> parse_program_options(int argc,
                                                           char** argv)
{
    // Declare the supported options.
    OptionsDescription desc("Allowed options");
    namespace po = boost::program_options;
    // clang-format off
    desc.add_options()
        ("help,h", "produce help message")
        ("font,f", po::value<std::filesystem::path>(), "set font path")
        ("shader, s", po::value<std::filesystem::path>(), "set shader path")
        ("x, x", po::value<int>(), "set x")
        ("y, y", po::value<int>(), "set y")
        ("tickrate, t", po::value<int>(), "set tickrate")
        ("delay,d", po::value<int>(), "set delay");
    // clang-format on

    // Store options and their values in the map.
    VariablesMap variables_map;
    po::store(po::parse_command_line(argc, argv, desc), variables_map);

    po::notify(variables_map);

    if (variables_map.count("help")) {
        fmt::print("{}\n", desc);
        return std::nullopt;
    }

    return FilePathConfiguration(variables_map);
}
}

int main(int argc, char** argv)
{
    try {
        auto configuration = parse_program_options(argc, argv);
        if (!configuration) { return 0; } // Help option.

        Window window("Server", 600, 600, configuration->font);
        Server server(std::move(window), *configuration);

        server.async_run();
    } catch (const std::exception& exc) {}
}
