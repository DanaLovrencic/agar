#ifndef SERVER_HPP
#define SERVER_HPP

#include "game/Game.hpp"
#include "game/PlayerController.hpp"
#include "Connection.hpp"
#include "ResponseQueue.hpp"
#include "ServerRenderer.hpp"

#include "Input.hpp"
#include "Window.hpp"
#include "game/GameRenderer.hpp"
#include "renderer/OrthographicCameraController.hpp"

class Server {
  private:
    using IOContext = boost::asio::io_context;
    using Acceptor  = boost::asio::ip::tcp::acceptor;
    using Endpoint  = boost::asio::ip::tcp::endpoint;
    using ErrorCode = boost::system::error_code;
    using Timer     = boost::asio::steady_timer;

    using MovementCommand = glm::vec2;

  private:
    /* Communication data. */
    static constexpr unsigned short port = 8080;
    //static constexpr unsigned tickrate   = 60; // Updates per second.
    static constexpr unsigned n_players  = 2;

    bool running = true; // For closing the window.

    IOContext io_context;
    // Stores player names and pointers to connections.
    std::unordered_map<std::string, std::shared_ptr<Connection>> connections;
    // Stores player names and movement commands from each player.
    std::unordered_map<std::string, std::vector<MovementCommand>>
        movement_commands;
    // Stores player names and queue that handles multiple writings.
    std::unordered_map<std::string, ResponseQueue> response_queues;

    /* Game logic data. */
    std::unique_ptr<Game> game;
    // Activates every 1/tickrate seconds to execute game step.
    Timer update_timer;

    /* Render data (only for demonstration purposes). */
    FilePathConfiguration path_config; // Stores path to the shader files, etc.
    Window window;
    Input input; // For input polling.

    std::unique_ptr<ServerRenderer> game_renderer;
    OrthographicCameraController camera_controller;

  public:
    explicit Server(Window window, const FilePathConfiguration& path_config);

    void async_run(); // Main (async) game "loop".

  private:
    void game_step();

    /* Action exectuted after async read operation. */
    void on_read_movement_command(MovementCommand movement_command,
                                  std::string name);

    /* Action exectuted after async wait operation. */
    void on_timer_expired();

    void accept(IOContext& io_context, std::size_t n_players);

    /* Handles common window events (resizing and closing). */
    void on_event(Event& event) noexcept;

    void update_game_state(std::chrono::milliseconds time);

    void register_async_reads();
    void update_player_positions();
    void send_game_state();

    /* Initialization. */
    std::vector<std::string> get_player_names();
    void send_initial_game_state();
    void
    initalize_movement_commands(const std::vector<std::string>& player_names);
    void initalize_response_queues();

    void stop_game();
};

#endif
